package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
//This application will function as an endpoint that will
//be used in handling http request.
@RestController
@RequestMapping("/greeting")
public class DiscussionApplication {
	ArrayList<String> enrollees=new ArrayList<>();
	ArrayList<Student> students=new ArrayList<>();
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
//		CaesarEncryptDecrypt("a z c d",5);
//		int [] key={4,5,1,3,6,2};
//		TranspositionEncryptDecrypt("giga chad ludi",key);

	}
	public static class Student{
		String id;
		String course;
		String name;

		public Student(String id, String course, String name) {
			this.id = id;
			this.course = course;
			this.name = name;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getCourse() {
			return course;
		}

		public void setCourse(String course) {
			this.course = course;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}


	}
	@GetMapping("/welcome")
	public String welcome(@RequestParam("user") String user,@RequestParam("role")String role){
		switch(role){
			case "teacher":
				return String.format("Thank you for Logging in Teacher, %s", user);
			case "admin":
				return String.format("Welcome back to the class portal Admin, %s",user);
			case "student":
				return String.format("Welcome to the class portal, %s",user);
			default:
				return String.format("role is out of range");

		}

	}
	@GetMapping("/register")
	public String register(@RequestParam("id") String id,@RequestParam("name")String name,@RequestParam("course") String course){
		Student s=new Student(id,course,name);
		students.add(s);
		return String.format("%s your id number is registered on the system", id);
	}
	@GetMapping("/account/{id}")
	public String account(@PathVariable("id")String id){
		int i=0;
		Student studentFound=null;
		for (Student student : students) {
			if (student.getId().equals(id)) {
				studentFound=student;
				break;
			}
		}
		if(studentFound!=null){
			return String.format("Welcome back %s! You are currently enrolled in %s",studentFound.getName(),studentFound.getCourse());
		}
		else{
			return String.format("Your provided %s is not found in the system",id);
		}
	}

	//localhost:8080/hello
	@GetMapping("/hello")
	//maps a get request to route /hello
	public String hello(){
		return "<h1>Hello World</h1>";
	}
	//route with string query
	//localhost:8080/hi?name=value
	@GetMapping("/hi")
	//@Request Param annotation that allows us to extract data from query strings in the URL
	public String hi(@RequestParam(value="name",defaultValue="John") String name){
		return String.format("Hi %s",name);
	}
	//Multiple parameters
	//localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	//@Request Param annotation that allows us to extract data from query strings in the URL
	public String friend(@RequestParam(value="name",defaultValue="Joe") String name, @RequestParam(value="friend",defaultValue="Juan") String friend){
		return String.format("Hi %s your friend is %s",name,friend);
	}

	//route with path variables
	//dynamic data is obtained directly from the url
	//localhost:8080/{name}
	@GetMapping("/hello/{name}")
	public String greetFriend(@PathVariable("name") String name){

		return String.format("Nce to meet you %s", name);
	}
	@GetMapping("/enroll")
	public String enroll(@RequestParam(value="user",defaultValue="Joe")String user){
		enrollees.add(user);
		return String.format("Thank you for enrolloing, %s", user);
	}
	@GetMapping("/getEnrollees")
	public ArrayList<String> getEnrollees(){

		return enrollees;
	}
	@GetMapping("/nameage")
	public String nameage(@RequestParam(value="name",defaultValue="Joe")String name,@RequestParam(value="age",defaultValue="18")String age){

		return String.format("Hello %s, my age is, %s", name,age);
	}
	@GetMapping("/courses/{id}")
	public String courses(@PathVariable("id")String id){
		String message="";
		if(id.equals("java101")){
			message="JAVA 101, MWF 8:00am-11:00am, PHP 3000.00";
		}
		else if(id.equals("sql101")){
			message="SQL 101, TTH 1:00pm-4:00pm, PHP 2000.00";
		}
		else if(id.equals("javaee101")){
			message="JAVA EE 101,MWF 1:00pm-4:00pm, PHP 3500.00";
		}
		else{
			message="course not found";
		}
		return message;
	}
	//code below is not included. just done for assignment
//	public static void CaesarEncryptDecrypt(String inphrase,int key){
//		System.out.print("\n **CAESAR CYPHER**");
//		String phrase=inphrase.toLowerCase();
//		String encrypted="";
//		int i=0;
//		System.out.print("\nyour original phrase:"+phrase);
//		//encryption part
//		for(i=0;i<phrase.length();i++){
//			if(phrase.charAt(i)==' '){
//				encrypted += ' ';
//			}else {
//				char x = phrase.charAt(i);
//				int encrpytedpos = (int) ((x + (int) key) % 'z');
//				if (encrpytedpos < 'a') {
//					encrpytedpos += 'a' - 1;
//				}
//				char letter = (char) encrpytedpos;
//				encrypted += letter;
//			}
//		}
//
//		System.out.print("\nyour encrypted phrase:"+encrypted);
//		//decryption part
//		String decrypted="";
//		for(i=0;i<encrypted.length();i++){
//			if(encrypted.charAt(i)==' '){
//				decrypted += ' ';
//			}else {
//				char x = encrypted.charAt(i);
//				int decrpytedpos = (int) ((x - (int) key));
//				if (decrpytedpos < 'a') {
//					int distance = 'a' - decrpytedpos;
//					decrpytedpos = 'z' - distance + 1;
//				}
//				char letter = (char) decrpytedpos;
//				decrypted += letter;
//			}
//		}
//		System.out.print("\nyour decrypted phrase:"+decrypted);
//	}
	//	public static void TranspositionEncryptDecrypt(String inputPhrase, int []key){
//		//remove spaces
//		String phrase=inputPhrase.replace(" ","");
//
//		//fill phrase with dummy value to fill in;
//		//encryption part below
//		int num=key.length;
//		int extra=num-(phrase.length()%num);
//
//		//if extra = 6 then just reset the extra to 0 kay di mn need moadd ug extra dummy chars
//		if(extra==num){
//			extra=0;
//		}
//		int i=0;
//		int j=0;
//		//padding with dummy characters
//		for(i=0;i<extra;i++){
//			phrase+="@";
//		}
//		//initializing encryption matrix
//
//		String []matrix=new String[num];
//		for(i=0;i< matrix.length;i++){
//			matrix[i]="";
//
//		}
//		//filling up encryption matrix
//
//		for(i=0;i<phrase.length();i++){
//			if(j>=num){
//				j=0;
//			}
//			matrix[key[j]-1]+=phrase.charAt(i);
//			j++;
//
//		}
//		String encrypted="";
//		//appending the encrypted text based on the key value and the matrix values
//		for(i=0;i<num;i++){
//
//			encrypted+=matrix[i];
//		}
//		System.out.print("\nyour encrypted phrase is:"+encrypted);
//
//
//
//
//
//
//
//		//**decryption part below
//		String []decryptionMatrix=new String[num];
//		for(i=0;i< decryptionMatrix.length;i++){
//			decryptionMatrix[i]="";
//
//		}
//		//decryption mapping part
//		for(i=0;i<phrase.length();i++){
//			if(j>=num){
//				j=0;
//			}
//			decryptionMatrix[key[j]-1]+=phrase.charAt(i);
//			j++;
//
//		}
//		//the part where we traverse the matrix from left to right and then append the letter to the decrypted text one by one;
//		String decrypted="";
//		for(i=0;i<encrypted.length()/num;i++){
//			for(j=0;j<num;j++){
//				decrypted+=decryptionMatrix[key[j]-1].charAt(i);
//			}
//
//		}
//		//remove the dummy characters
//		decrypted=decrypted.replace("@","");
//		System.out.print("\ndecryption matrix:"+decrypted);
//	}


}
